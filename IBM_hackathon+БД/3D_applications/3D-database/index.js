"use strict";

let express = require("express");
let app = express();
let pg = require('pg');

function createNewClient() {

    /*
    return new pg.Client({
        user: 'postgres',
        host: 'localhost',
        database: 'b12345',
        password: '123',
        port: 5432
    });
    */

    return new pg.Client({
        connectionString: process.env.DATABASE_URL,
        ssl: true
    });
}

function sendQuery(query, result, callback) {
    // create client object
    let client = createNewClient();
    // open connection
    client.connect();

    client.query(query.toString(), (err, res) => {
        // save answer
        result.arr = res.rows;
        // close connection
        client.end();
        // call callback
        callback();
    });
}

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function(request, response) {
    response.end("HELLO__________I_AM_SERVER_WITH_DATABASE");
    console.log("HELLO__________I_AM_SERVER_WITH_DATABASE");
});

app.get('/create_table', function(request, response) {
    console.log("CREATE_TABLE");
    let query = " CREATE TABLE IF NOT EXISTS results (result_id SERIAL PRIMARY KEY, level_array TEXT, level_name TEXT, user_name TEXT, game_result TEXT); ";
    sendQuery(query, {}, () => {
       response.end("CREATE_TABLE_OK");
    });
});

app.get('/delete_all', function(request, response) {
    console.log("DELETE_ALL");
    let query = " DELETE FROM results; ";
    sendQuery(query, {}, () => {
       response.end("DELETE_ALL_OK");
    });
});

app.get('/get_all', function(request, response) {
    console.log("GET_ALL");

    let query = " SELECT * FROM results ORDER BY result_id; ";

    let result = {
        arr: []
    };

    sendQuery(query, result, () => {
        response.end(JSON.stringify(result.arr));
    });
});

app.post('/add_data', function(request, response) {
    console.log("ADD_DATA");

    let bigString = "";
    request.on('data', (data) => {
        bigString += data.toString();
    }).on('end', () => {
        let obj = JSON.parse(bigString);

        const level_array = obj.level_array + "";
        const level_name = obj.level_name + "";
        const user_name = obj.user_name + "";
        const game_result = obj.game_result + "";

        let result = {
            arr: []
        };

        let query = " INSERT INTO results (level_array, level_name, user_name, game_result) VALUES ('" + level_array + "', '" + level_name + "', '" + user_name + "', '" + game_result + "'); ";
        sendQuery(query, {}, () => {
            response.end("ADD_DATA_OK");
        });
    });
});

let port = process.env.PORT || 5005;
app.listen(port);
console.log("Server works on port " + port);


